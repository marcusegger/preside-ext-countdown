<cfscript>
	event.include( "ext-preside-jq-js" ).include( "ext-preside-bs4-js" ).include( "ext-preside-bs4-css" ).include( "ext-preside-countdown-css" );
</cfscript>

<cfif args.active>

	<cfoutput>

		<style>

			.ext-preside-countdown-titlecolor {
				color:  #args.titleColor#;	
			}		

			.ext-preside-countdown-unitcolor {
				color:  #args.unitColor#;	
			}

			.ext-preside-countdown-valuecolor {
				color:  #args.valueColor#;	
			}			

		</style>



		<div class="container-fluid px-0 mr-auto" id="countdownInvert" style="background-color: #args.backgroundColor#">
			<div class="m-0 changeableCont">

				<!--- optional title/headline --->
				<cfif len(args.title)>
					<div class="row p-5">
						<div class="col-md-12 text-center"><p class="ext-preside-countdown-title ext-preside-countdown-titlecolor">#args.title#</p></div>
					</div>
				</cfif>

				<div class="row ext-preside-countdown-unit ext-preside-countdown-unitcolor">
					<div class="col-3 text-center">Tage</div>
					<div class="col-3 text-center">Stunden</div>
					<div class="col-3 text-center">Minuten</div>
					<div class="col-3 text-center">Sekunden</div>								
				</div>

				<div class="row ext-preside-countdown-value ext-preside-countdown-valuecolor pb-5">
					<div class="col-3 text-center" id="daysLarge"></div>
					<div class="col-3 text-center" id="hoursLarge"></div>
					<div class="col-3 text-center" id="minutesLarge"></div>
					<div class="col-3 text-center" id="secondsLarge"></div>				
				</div>			

			</div>
		</div>

		<script>
			const second = 1000,
				minute = second * 60,
				hour = minute * 60,
				day = hour * 24;

			let countDown = new Date('#args.endDate#').getTime(),
				x = setInterval(function() {

				let now = new Date().getTime(),
						distance = countDown - now;

						//do something later when date is reached
						if (distance < 0) {
						clearInterval(x);
						$('.changeableCont').html('<div class="text-center">#args.contentAfterCountdown#</div>');
						return;
						}            

						let myMinutes = Math.floor((distance % (hour)) / (minute));
						let myHours = Math.floor((distance % (day)) / (hour));
						let myDays = Math.floor(distance / (day));
						let mySeconds = Math.floor((distance % (minute)) / second);

						myMinutesStr = myMinutes.toString().padStart(2, '0');
						myHoursStr = myHours.toString().padStart(2, '0');
						myDaysStr = myDays.toString().padStart(2, '0');
						mySecondsStr = mySeconds.toString().padStart(2, '0');
						
						document.getElementById('daysLarge').innerText = myDaysStr,
						document.getElementById('hoursLarge').innerText = myHoursStr,
						document.getElementById('minutesLarge').innerText = myMinutesStr;
						document.getElementById('secondsLarge').innerText = mySecondsStr;
						
						//document.getElementById('daysSmall').innerText = myDaysStr + " : ",
						//document.getElementById('hoursSmall').innerText = myHoursStr + " : ",
						//document.getElementById('minutesSmall').innerText = myMinutesStr;
						//document.getElementById('secondsSmall').innerText = mySecondsStr;              

				}, second)	
		</script>

	</cfoutput>
</cfif>