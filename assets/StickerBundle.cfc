component {

	public void function configure( bundle ) {

		bundle.addAsset( id="ext-preside-jq-js", url="https://code.jquery.com/jquery-3.5.1.slim.min.js" );
		bundle.addAsset( id="ext-preside-bs4-js", url="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" );
        bundle.addAsset( id="ext-preside-bs4-css", url="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" );
		bundle.addAsset( id="ext-preside-countdown-css"  , path="/css/frontend/countdown.css" );       

	}

}



