# Countdowns for Preside

This extension allows you to add countdown elements in your Preside application. Simply add the widget in your richtext field, and it will be displayed in the front end.